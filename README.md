## Presentation
Kata from https://github.com/emilybache/GildedRose-Refactoring-Kata

## Install
```
npm install
```

## Run

```
npm run test
```
