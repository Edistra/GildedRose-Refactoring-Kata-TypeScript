import { expect } from 'chai';
import { Inn } from './Inn';
import { Item } from './Item/Item';

describe('Gilded Rose', function () {
    it('should init properly', function () {
        const gildedRose = new Inn([new Item('foo', 99, 0)]);
        const items = gildedRose.items;
        expect(items[0].name).to.equal('foo');
        expect(items[0].sellIn).to.equal(99);
        expect(items[0].quality).to.equal(0);
    });

    describe('updateQuality | default', function () {
        it('should decrease sellIn and quality', function () {
            const gildedRose = new Inn([new Item('foo', 99, 1)]);
            const items = gildedRose.updateQuality();
            expect(items[0].sellIn).to.equal(98);
            expect(items[0].quality).to.equal(0);
        });
        it('should decrease only sellIn if quality already at 0', function () {
            const gildedRose = new Inn([new Item('foo', 99, 0)]);
            const items = gildedRose.updateQuality();
            expect(items[0].sellIn).to.equal(98);
            expect(items[0].quality).to.equal(0);
        });

        it('should decrease quality twice as normal if sellIn < 0', function () {
            const gildedRose = new Inn([new Item('foo', 0, 2)]);
            const items = gildedRose.updateQuality();
            expect(items[0].sellIn).to.equal(-1);
            expect(items[0].quality).to.equal(0);
        });
    });
});
