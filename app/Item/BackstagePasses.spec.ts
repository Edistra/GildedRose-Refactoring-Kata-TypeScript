import { expect } from 'chai';
import { BackstagePasses } from "./BackstagePasses";

describe('updateQuality | Backstage passes to a TAFKAL80ETC concert', function () {
    it('should increase quality by 1 if sellIn >= 11', function () {
        const item = new BackstagePasses('Backstage passes to a TAFKAL80ETC concert', 11, 5);
        item.update();
        expect(item.sellIn).to.equal(10);
        expect(item.quality).to.equal(6);
    });
    it('should increase quality by 2 if sellIn >= 6 and < 11', function () {
        const item = new BackstagePasses('Backstage passes to a TAFKAL80ETC concert', 10, 5);
        item.update();
        expect(item.sellIn).to.equal(9);
        expect(item.quality).to.equal(7);
    });
    it('should increase quality by 3 if sellIn < 6', function () {
        const item = new BackstagePasses('Backstage passes to a TAFKAL80ETC concert', 5, 5);
        item.update();
        expect(item.sellIn).to.equal(4);
        expect(item.quality).to.equal(8);
    });
    it('should set quality to 0 if sellIn < 0', function () {
        const item = new BackstagePasses('Backstage passes to a TAFKAL80ETC concert', 0, 5);
        item.update();
        expect(item.sellIn).to.equal(-1);
        expect(item.quality).to.equal(0);
    });
});
