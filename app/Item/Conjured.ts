import {Item} from "./Item";

export class Conjured extends Item {
    decreaseQuality(): void {
        super.decreaseQuality(2);
    }
}