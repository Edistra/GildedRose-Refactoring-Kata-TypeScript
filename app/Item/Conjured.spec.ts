import { expect } from 'chai';
import { Conjured } from './Conjured';

describe('Conjured | update', function () {
    it('should decrease sellIn by 1 and quality by 2', function () {
        const item = new Conjured('foo', 99, 2);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(0);
    });
    it('should not decrease quality under 0', function () {
        const item = new Conjured('foo', 99, 1);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(0)
    });
    it('should decrease only sellIn if quality already at 0', function () {
        const item = new Conjured('foo', 99, 0);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(0)
    });
    it('should decrease quality twice as normal if sellIn < 0', function () {
        const item = new Conjured('foo', 0, 4);
        item.update();
        expect(item.sellIn).to.equal(-1);
        expect(item.quality).to.equal(0)
    });
});
