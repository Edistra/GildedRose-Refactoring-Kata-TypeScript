import {Item} from "./Item";

export class BackstagePasses extends Item {
    getIncreaseQualityFactor(): number {
        let result = 1;
        if (this.sellIn < 6) {
            result = 3;
        } else if (this.sellIn < 11) {
            result = 2;
        }
        return result;
    }

    applyPreDecreaseSellInEffect(): void {
        this.increaseQuality(this.getIncreaseQualityFactor());
    }

    applyNegativeSellInEffect(): void {
        this.quality = 0;
    }
}