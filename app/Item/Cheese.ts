import {Item} from "./Item";

export class Cheese extends Item {
    applyPreDecreaseSellInEffect(): void {
        this.increaseQuality();
    }

    applyNegativeSellInEffect(): void {
        this.increaseQuality();
    }
}