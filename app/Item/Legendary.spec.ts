import { expect } from 'chai';
import { Legendary } from './Legendary';

describe('Legendary | update', function () {
    it('should not affect quality or sellIn in any case', function () {
        const item = new Legendary('Sulfuras, Hand of Ragnaros', 99);
        item.update();
        expect(item.sellIn).to.equal(99);
        expect(item.quality).to.equal(80);
    });
});
