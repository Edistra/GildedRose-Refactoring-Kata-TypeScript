export class Item {
    public static readonly MIN_QUALITY: number = 0;
    public static readonly MAX_QUALITY: number = 50;
    public static readonly QUALITY_STEP: number = 1;
    public static readonly MIN_SELLIN: number = 0;
    public static readonly SELLIN_STEP: number = 1;
    
    name: string;
    sellIn: number;
    quality: number;

    constructor(name, sellIn, quality) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
    
    increaseQuality(factor: number = 1): void {
        this.quality = this.quality + Item.QUALITY_STEP * factor;
        if (this.quality > Item.MAX_QUALITY) {
            this.quality = Item.MAX_QUALITY;
        }
    }

    decreaseQuality(factor: number = 1): void {
        this.quality -= Item.QUALITY_STEP * factor;
        if (this.quality < Item.MIN_QUALITY) {
            this.quality = Item.MIN_QUALITY;
        }
    }

    decreaseSellIn(): void {
        this.sellIn -= Item.SELLIN_STEP;
    }

    applyPreDecreaseSellInEffect(): void {
        this.decreaseQuality();
    }

    applyNegativeSellInEffect(): void {
        this.decreaseQuality();
    }
    
    update(): void {
        this.applyPreDecreaseSellInEffect();
        this.decreaseSellIn();
        if (this.sellIn < Item.MIN_SELLIN) {
            this.applyNegativeSellInEffect();
        }
    }
}