import { expect } from 'chai';
import { Cheese } from "./Cheese";

describe('updateQuality | Aged Brie', function () {
    it('should increase quality if < 50', function () {
        const item = new Cheese('Aged Brie', 99, 0);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(1);
    });
    it('should not increase quality if >= 50', function () {
        const item = new Cheese('Aged Brie', 99, 50);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(50);
    });
    it('should increase quality twice as normal if sellIn < 0', function () {
        const item = new Cheese('Aged Brie', 0, 0);
        item.update();
        expect(item.sellIn).to.equal(-1);
        expect(item.quality).to.equal(2);
    });
});
