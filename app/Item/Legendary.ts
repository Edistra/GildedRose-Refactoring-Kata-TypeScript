import {Item} from "./Item";

export class Legendary extends Item {
    public static readonly MAX_QUALITY: number = 80;

    constructor(name, sellIn) {
        super(name, sellIn, Legendary.MAX_QUALITY);
    }

    update() {
        // does nothing
    }
}