import { expect } from 'chai';
import { Item } from './Item';

describe('Item | update', function () {
    it('should decrease sellIn and quality', function () {
        const item = new Item('foo', 99, 1);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(0);
    });
    it('should decrease only sellIn if quality already at 0', function () {
        const item = new Item('foo', 99, 0);
        item.update();
        expect(item.sellIn).to.equal(98);
        expect(item.quality).to.equal(0)
    });

    it('should decrease quality twice as normal if sellIn < 0', function () {
        const item = new Item('foo', 0, 2);
        item.update();
        expect(item.sellIn).to.equal(-1);
        expect(item.quality).to.equal(0)
    });
});
