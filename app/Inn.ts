import {Item} from "./Item/Item";

export class Inn {
    items: Array<Item>;

    constructor(items = []) {
        this.items = items;
    }

    updateQuality() {
        this.items.forEach( (item: Item) => item.update());
        return this.items;
    }
}